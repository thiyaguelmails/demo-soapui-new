## Objective ##

To publish the test results (JUnit, Cucumber, Robot Framework) on JIRA XRay.  

## XRay Integration ##
 
XRay API's help us to publish Test results on JIRA. Way to publish the results on JIRA varied based on Testing frameworks. We will read about it now. 

### JUnit ###

XRay creates new JIRA Test & Test execution Issues based on the test cases described in JUnit results.  

After the test execution, Following curl command may help to upload JUnit Test results to XRay.

	curl -H "Content-Type: multipart/form-data" -u <JIRA_USERNAME>:<JIRA_PASSWORD> -F "file=@<PATH_OF_JUNIT_RESULTS_FILE>" "<JIRA_HOST_URL>/rest/raven/1.0/import/execution/junit?projectKey=<JIRA_PROJECT_KEY>"

Example to refer in Rosetta project for this functionality - [https://gitlab.rosetta.ericssondevops.com/examples/rosetta-demo-blog/blob/master/.gitlab-ci.yml#L49](https://gitlab.rosetta.ericssondevops.com/examples/rosetta-demo-blog/blob/master/.gitlab-ci.yml#L49)


### Cucumber ###

Cucumber testcase execution is bit different from JUnit. In Junit case, we may need to write Test cases at Project space, based on the test results, XRay will create new JIRA Test & Test execution Issues. 

In case of Cucumber, either we have to tag the JIRA issue ID's in local testcase, or We have to store the testcases in JIRA Test issue then create a Test execution issue by associating created test issues. Secondly We may need to pull the testcases from JIRA before running test. Post test execution we can push the test results to XRay.

Pulling Testcases from JIRA :
	
	'curl -u <JIRA_USERNAME>:<JIRA_PASSWORD> "<JIRA_HOST_URL>/rest/raven/1.0/export/test?keys=<JIRA_TEST_EXECUTION_ISSUE_ID>" -o <PATH_TO_PLACE_TESTCASES_AT_PROJECT_FOLDER>'

Example to refer in Rosetta project for this functionality - [https://gitlab.rosetta.ericssondevops.com/examples/rosetta-demo-cucumber/blob/master/.gitlab-ci.yml#L30](https://gitlab.rosetta.ericssondevops.com/examples/rosetta-demo-cucumber/blob/master/.gitlab-ci.yml#L30)

After the test execution, Following curl command may help to upload Cucumber Test results to XRay.

	curl -H "Content-Type: application/json" -u <JIRA_USERNAME>:<JIRA_PASSWORD> --data @<PATH_OF_CUCUMBER_TEST_RESULTS_FILE> "<JIRA_HOST_URL>/rest/raven/1.0/import/execution/cucumber

Example to refer in Rosetta project for this functionality - [https://gitlab.rosetta.ericssondevops.com/examples/rosetta-demo-cucumber/blob/master/.gitlab-ci.yml#L33](https://gitlab.rosetta.ericssondevops.com/examples/rosetta-demo-cucumber/blob/master/.gitlab-ci.yml#L33)


### Robot Framework ###

Similar to JIRA, XRay creates new JIRA Test & Test execution Issues based on the test cases described in Robot Test results.  

After the test execution, Following curl command may help to upload Robot Framework Test results to XRay.

	curl -H "Content-Type: multipart/form-data" -u <JIRA_USERNAME>:<JIRA_PASSWORD> -F "file=@<PATH_OF_ROBOT_FRAMEWORK_RESULTS_FILE>" "<JIRA_HOST_URL>/rest/raven/1.0/import/execution/robot?projectKey=<JIRA_PROJECT_KEY>"

Example to refer in Rosetta project for this functionality - [https://gitlab.rosetta.ericssondevops.com/examples/rosetta-demo-robotframework-maven](https://gitlab.rosetta.ericssondevops.com/examples/rosetta-demo-robotframework-maven/blob/master/.gitlab-ci.yml#L20)


### Other Testing Frameworks ###

Other than JUnit, Cucumber & Robot Framework, XRay supports - NUnit, TestNG & Behave JSON frameworks. Refer the [link](https://confluence.xpand-it.com/display/XRAY/Import+Execution+Results+-+REST)

If we wish to use anyother testing frameworks other than specified 6 testing frameworks, We may need to integrate the target frame along with one of the supported framework to publish test results on XRay.

We had integrated SoapUI test project with Junit to publish SoapUI Test results to XRay at [Rosetta Project](https://gitlab.rosetta.ericssondevops.com/examples/rosetta-demo-soapui)


Reference : 

[https://confluence.xpand-it.com/display/XRAY/Integration+with+GitLab#IntegrationwithGitLab-JUnitexample](https://confluence.xpand-it.com/display/XRAY/Integration+with+GitLab#IntegrationwithGitLab-JUnitexample)
[https://confluence.xpand-it.com/pages/viewpage.action?pageId=21761014](https://confluence.xpand-it.com/pages/viewpage.action?pageId=21761014)
[https://confluence.xpand-it.com/display/XRAY/Import+Execution+Results+-+REST](https://confluence.xpand-it.com/display/XRAY/Import+Execution+Results+-+REST)

